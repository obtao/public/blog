ARG PHP_VERSION=1.0.1
ARG NGINX_VERSION=1.0.5

FROM registry.gitlab.com/obtao/docker-images/php-fpm:${PHP_VERSION} AS blog_php

WORKDIR /srv/app
ARG APP_ENV=prod

COPY composer.json composer.lock ./
RUN set -eux; \
	composer install --prefer-dist --no-dev --no-autoloader --no-scripts --no-progress --no-suggest; \
	composer clear-cache

COPY . ./

RUN set -eux; \
	mkdir -p var/cache var/log; \
	composer dump-autoload --classmap-authoritative --no-dev; \
	composer run-script --no-dev post-install-cmd; \
	chown -R www-data:www-data var/cache var/log; \
	chmod +x bin/console; sync


ARG APP_VERSION=APP_VERSION
ARG CI_JOB_STAGE=CI_JOB_STAGE
ARG CI_COMMIT_REF=CI_COMMIT_REF

ENV APP_VERSION $APP_VERSION
ENV CI_JOB_STAGE $CI_JOB_STAGE
ENV CI_COMMIT_REF_NAME $CI_COMMIT_REF_NAME

USER www-data

FROM registry.gitlab.com/obtao/docker-images/nginx:${NGINX_VERSION} AS blog_nginx

WORKDIR /srv/app/

COPY --from=blog_php /srv/app/public public/

USER nginx
