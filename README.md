# Blog (Demo Docker/Kubernetes)

## Principe 

Le but de ce dépôt est d'expliquer la configuration docker locale et le déploiement Kubernetes d'une application Symfony.
Il contient : 

* Un faux Blog en Symfony 5 (Catégories, Articles, Auteurs et une page liste)
* La configuration pour développer en local sous Docker (avec `traefik`)
* La configuration pour utiliser Gitlab CI/CD
* La configuration pour déployer dans un cluster Kubernetes sous Helm
* La configuration Monolog pour une remontée automatique par la stack `EFK`
* La configuration pour utiliser prometheus (exporters `nginx`, `php-fpm` et `query (postgresql)`)

Il utilise les 2 images de base `php-fpm` et `nginx`.

Vous pouvez le copier à loisir et l'améliorer si besoin. 

## Local 

En local, nous utilisons : 
* [traefik](https://traefik.io/) pour éviter le mapping de ports et gérer des noms de domaine `*.localhost`. 
* `docker-compose` pour lancer les containers


**Si vous intégrez plusieurs projets** , nous vous conseillons de : 
1. externaliser le `traefik` et de le démarrer à chaque démarrage de docker, et lui créer un network `traefik`
2. gérer 1 network docker par application pour les cloisonner
3. Ajouter un alias à vos containers `nginx` dans le réseau `traefik` pour qu'ils puissent être exposés. 


## Helm / Kubernetes

Ce dépôt contient les charts Helm permettant de déployer l'application dans un cluster Kubernetes. 

Ce chart intègre : 

* Un *Deployment* `php-fpm` (et les *ConfigMap*/*Secret*/*Service* associés)
* Un *Deployment* `nginx` (et les *ConfigMap*/*Service* associés)
* Une dépendance vers un PostgreSQL
* Un *Ingress* pointant vers le *Service* Nginx
* 2 *ServiceMonitor*  pour récupérer les métriques du `php-fpm-exporter` et `nginx-exporter` dans Prometheus
* Un *ConfigMap* qui contient un Dashboard pour Grafana.

## Logs

Le dépôt contient la configuration monolog et les *Processor* qui ajoutent aux champs *extras* de chaque log : 

* **AppVersion** : Version de l'application qui génère les logs (Cf. [Dockerfile](./Dockerfile))
* **RequestId** : `uuid` de la request, généré par Nginx permettant de suivre et aggréger les logs par `Request`

Ces logs sont au format JSON pour une intégration dans Elasticsearch par Fluentd. 

## Gitlab-ci

Côté déploiement et intégration continue, ce dépôt embarque la configuration pour Gitlab. 

### Build

Le build automatique des images se fait sur : 
* `master` : le nom de l'image sera `latest`
* `tag` : le nom de l'image sera le nom du `tag`
* les branches `feature/*`, `hotfix/*`, `release/*`


### Tests
 
Pour le moment, la CI ne teste que `php-cs`. Pas parce que le checkstyle est important, mais parce que c'est un exemple simple. 
Vous pourrez ajouter dans ce job les tests unitaires, les tests fonctionnels, des tests Blackfire, ...

### Scan d'image et de dépendances

La CI vient tester la sécurité sur les images docker et les dépendances de composer. (Merci @Matiti ! )

### Déploiement

Ce job gère le déploiement et la destruction d'un environnement.
Vous pourriez en gérer autant que vous le voulez selon la complexité de vos applications. 
