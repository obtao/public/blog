<?php

namespace App\Command;

use App\Entity\Article;
use App\Entity\Author;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Generator;
use Faker\Provider\Base;
use Faker\Provider\en_US\Text;
use Faker\Provider\Person;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateBlogPostCommand extends Command
{
    /**
     * @var Generator
     */
    private $fakerGenerator;

    /**
     * @var Text
     */
    private $fakerString;

    /**
     * @var Person
     */
    private $fakerAuthor;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
     */
    private $categoryRepository;

    /**
     * @var Base
     */
    private $fakerBase;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(Generator $fakerGenerator, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->fakerGenerator = $fakerGenerator;
        $this->fakerString = new Text($fakerGenerator);
        $this->fakerAuthor = new Person($fakerGenerator);
        $this->fakerBase = new Base($fakerGenerator);
        $this->categoryRepository = $em->getRepository(Category::class);
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this
            ->setName('blog:create-post')
            ->addArgument('count', InputArgument::REQUIRED, 'Count of blog post to create');
    }

    /**
     * @return int|void|null
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = $input->getArgument('count');

        for ($i = 0; $i < $count; ++$i) {
            $author = new Author();
            $author->setName($this->fakerAuthor->firstName());
            $this->em->persist($author);

            $category = $this->getRandomCategory();

            $article = new Article();
            $article->setTitle($this->fakerString->realText(40));
            $article->setContent($this->fakerString->realText());
            $article->setAuthor($author);
            $article->setCategory($category);
            $this->em->persist($article);
            $this->em->flush();
        }

        return 0;
    }

    /**
     * @return Category
     */
    private function getRandomCategory()
    {
        $categoryTitle = $this->fakerBase::randomElement(Category::$categories);
        $category = $this->categoryRepository->findOneBy(['title' => $categoryTitle]);
        if (null === $category) {
            $category = new Category();
            $category->setTitle($categoryTitle);
            $this->em->persist($category);
        }

        return $category;
    }
}
