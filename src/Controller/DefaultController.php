<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController
{
    /**
     * @Route("/")
     * @Template()
     */
    public function home(EntityManagerInterface $em)
    {
        $articles = $em->getRepository(Article::class)->findBy([], null, 10);

        return [
            'articles' => $articles,
        ];
    }
}
