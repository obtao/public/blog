<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Article.
 *
 * @ORM\Entity()
 */
class Article
{
    /**
     * @var int
     * @ORM\GeneratedValue
     * @ORM\Id
     * @ORM\Column
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="articles")
     */
    private $category;

    /**
     * @var Author
     * @ORM\ManyToOne(targetEntity="Author", inversedBy="articles")
     */
    private $author;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Article
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Article
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): Article
    {
        $this->content = $content;

        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): Article
    {
        $this->category = $category;

        return $this;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function setAuthor(Author $author): Article
    {
        $this->author = $author;

        return $this;
    }
}
