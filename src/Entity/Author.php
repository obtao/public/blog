<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Author.
 *
 * @ORM\Entity()
 */
class Author
{
    /**
     * @var int
     * @ORM\GeneratedValue
     * @ORM\Id
     * @ORM\Column
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Article[]
     * @ORM\OneToMany(targetEntity="Article", mappedBy="author")
     */
    private $articles;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Author
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Author
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Article[]
     */
    public function getArticles(): array
    {
        return $this->articles;
    }

    /**
     * @param Article[] $articles
     */
    public function setArticles(array $articles): Author
    {
        $this->articles = $articles;

        return $this;
    }
}
