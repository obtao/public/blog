<?php

namespace App\Logger;

/**
 * Class AppVersionProcessor.
 */
class AppVersionProcessor
{
    /** @var string $appVersion */
    private $appVersion;

    /**
     * RequestIdProcessor constructor.
     */
    public function __construct(string $appVersion)
    {
        $this->appVersion = $appVersion;
    }

    /**
     * @return array
     */
    public function __invoke(array $record)
    {
        $record['extra']['app_version'] = $this->appVersion;

        return $record;
    }
}
