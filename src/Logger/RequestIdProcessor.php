<?php

namespace App\Logger;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RequestIdProcessor.
 */
class RequestIdProcessor
{
    /** @var RequestStack $requestStack */
    private $requestStack;

    /**
     * RequestIdProcessor constructor.
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @return array
     */
    public function __invoke(array $record)
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request) {
            return $record;
        }

        try {
            $record['extra']['request_id'] = $request->server->get('REQUEST_ID');
        } catch (\Exception $e) {
            $record['extra']['request_id'] = $e->getMessage();
        }

        return $record;
    }
}
