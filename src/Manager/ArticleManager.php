<?php

namespace App\Manager;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ArticleManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @return Article
     */
    public function saveArticle(Article $article)
    {
        $this->entityManager->persist($article);
        $this->entityManager->flush();

        $this->logger->info("'{articleTitle}' article added by {author} in category '{category}'", [
            'articleTitle' => $article->getTitle(),
            'author' => $article->getAuthor()->getName(),
            'category' => $article->getCategory()->getTitle(),
        ]);

        return $article;
    }
}
